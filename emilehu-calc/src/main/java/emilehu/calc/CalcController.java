package emilehu.calc;

import java.util.List;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.Labeled;
import javafx.scene.control.ListView;

public class CalcController {

    private Calc calc;

    public CalcController() {
        calc = new Calc(0.0, 0.0, 0.0);
    }

    public Calc getCalc() {
        return calc;
    }

    public void setCalc(Calc calc) {
        this.calc = calc;
        updateOperandsView();
    }

    @FXML
    private ListView<Double> operandsView;

    @FXML
    private Label operandView;

    @FXML
    void initialize() {
        updateOperandsView();
    }

    private void updateOperandsView() {
        List<Double> operands = operandsView.getItems();
        operands.clear();
        int elementCount = Math.min(calc.getOperandCount(), 3);
        for (int i = 0; i < elementCount; i++) {
            operands.add(calc.peekOperand(elementCount - i - 1));
        }
    }

    private String getOperandString() {
        return operandView.getText();
    }

    private boolean hasOperand() {
        return ! getOperandString().isBlank();
    }

    private double getOperand() {
        return Double.valueOf(operandView.getText());
    }
    
    private void setOperand(String operandString) {
        operandView.setText(operandString);
    }

    @FXML
    void handleEnter() {
        if (hasOperand()) {
            calc.pushOperand(getOperand());
        } else {
            calc.dup();
        }
        setOperand("");
        updateOperandsView();
    }

    private void appendToOperand(String s) {
         setOperand(getOperand() + s;)
         updateOperandsView();
    }

    @FXML
    void handleDigit(ActionEvent ae) {
        if (ae.getSource() instanceof Labeled l) {
            // TODO append button label to operand
            String label = l.getText();
            appendToOperand(label);
        }
        updateOperandsView();
    }

    @FXML
    void handlePoint() {
        var operandString = getOperandString();
        if (operandString.contains(".")) {
            // TODO remove characters after point
            String s = operandStack.split("\\.")[0] + ".";
            setOperand(s);
        } else {
            appendToOperand(".")
        }
        updateOperandsView();
    }

    @FXML
    void handleClear() {
        String empty = "";
        setOperand(empty);
        updateOperandsView();
    }

    @FXML
    void handleSwap() {
        calc.swap();
        updateOperandsView();
    }

    private void performOperation(UnaryOperator<Double> op) {
        calc.performOperation(op);
    }

    private void performOperation(boolean swap, BinaryOperator<Double> op) {
        if (hasOperand()) {
           appendToOperand(getOperandString())
        }
        if(swap){
            calc.swap();
        }
        calc.performOperation(op)
    }

    @FXML
    void handleOpAdd() {
        BinaryOperator<Double> add = (a, b) -> a + b;
        performOperation(false, add);
        updateOperandsView();
    }

    @FXML
    void handleOpSub() {
        BinaryOperator<Double> sub = (a, b) -> a - b;
        performOperation(false, sub);
        updateOperandsView();
    }

    @FXML
    void handleOpMult() {
        BinaryOperator<Double> mult = (a, b) -> a * b;
        performOperation(false, mult);
        updateOperandsView();
    }

    @FXML
    void handleOpDiv() {
        BinaryOperator<Double> div = (a, b) -> a / b;
        performOperation(false, div);
        updateOperandsView();
    }

    
    @FXML
    void handleOpSqr() {
        UnaryOperator<Double> sqr = (a) -> Math.sqrt(a);
        performOperation(sqr);
        updateOperandsView();
        }
    
    @FXML
    void handlePi(ActionEvent ae) {
        String s = "";
        operandView.setText(s + String.valueOf(Math.PI));
        handleEnter();
}
